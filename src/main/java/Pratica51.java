import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;
import utfpr.ct.dainf.if62c.pratica.MatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.SomaMatrizesIncompativeisException;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica51 {

    public static void main(String[] args) throws MatrizInvalidaException {
        try{
            Matriz orig = new Matriz(3, 2);
            Matriz origsoma = new Matriz(3, 2);
            Matriz origmult = new Matriz(2, 3);
            double[][] m = orig.getMatriz();
            double[][] ms = origsoma.getMatriz();
            double[][] mm = origmult.getMatriz();
            m[0][0] = 0.0;
            m[0][1] = 0.1;
            m[1][0] = 1.0;
            m[1][1] = 1.1;
            m[2][0] = 2.0;
            m[2][1] = 2.1;

            ms[0][0] = 0.0;
            ms[0][1] = 0.1;
            ms[1][0] = 1.0;
            ms[1][1] = 1.1;
            ms[2][0] = 2.0;
            ms[2][1] = 2.1;

            mm[0][0] = 0.0;
            mm[0][1] = 0.1;
            mm[0][2] = 1.0;
            mm[1][0] = 1.1;
            mm[1][1] = 2.0;
            mm[1][2] = 2.1;
            Matriz transp = orig.getTransposta();
            System.out.println("Matriz original: " + orig);
            System.out.println("Matriz transposta: " + transp);
            System.out.println("Matriz Somada: " + orig.soma(origsoma));
            System.out.println("Matriz Somada 2: " + orig.soma(origmult));
            System.out.println("Matriz Multiplicada: " + orig.prod(origmult));
            System.out.println("Matriz Multiplicada 2: " + origmult.prod(origmult));
        } catch (ProdMatrizesIncompativeisException pmie) {
            System.out.println("Ocorreu um erro de Produto de Matrizes Incompativeis: "
                + pmie.getLocalizedMessage());
        } catch (SomaMatrizesIncompativeisException smie) {
            System.out.println("Ocorreu um erro de Soma de Matrizes Incompativeis: "
                + smie.getLocalizedMessage());
        }
    }
}
