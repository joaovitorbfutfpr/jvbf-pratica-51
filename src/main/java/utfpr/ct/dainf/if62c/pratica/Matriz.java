package utfpr.ct.dainf.if62c.pratica;

/**
 * Representa uma matriz de valores {@code double}.
 * @author Wilson Horstmeyer Bogadao <wilson@utfpr.edu.br>
 */
public class Matriz {
    
    // a matriz representada por esta classe
    private final double[][] mat;
    
    /**
     * Construtor que aloca a matriz.
     * @param m O número de linhas da matriz.
     * @param n O número de colunas da matriz.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz(int m, int n) throws MatrizInvalidaException{
        if (m<=0 || n<=0) {
            throw new MatrizInvalidaException(this);
        }
        mat = new double[m][n];
    }
    
    /**
     * Retorna a matriz representada por esta classe.
     * @return A matriz representada por esta classe
     */
    public double[][] getMatriz() {
        return mat;
    }
    
    /**
     * Retorna a matriz transposta.
     * @return A matriz transposta.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz getTransposta() throws MatrizInvalidaException {
        Matriz t = new Matriz(mat[0].length, mat.length);
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                t.mat[j][i] = mat[i][j];
            }
        }
        return t;
    }
    
    /**
     * Retorna a soma desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser somada
     * @return A soma das matrizes
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz soma(Matriz m) throws MatrizInvalidaException {
        Matriz sum = new Matriz(mat.length, mat[0].length);
        if (mat.length != m.mat.length || mat[0].length != m.mat[0].length){
            throw new SomaMatrizesIncompativeisException(this, m);
        }
        for (int i = 0; i < mat.length; i++){
            for (int j = 0; j < mat[i].length; j++){
                sum.mat[i][j] = mat[i][j] + m.mat[i][j];
            }
        }
        return sum;
    }

    /**
     * Retorna o produto desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser multiplicada
     * @return O produto das matrizes
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz prod(Matriz m) throws MatrizInvalidaException {
        Matriz mult = new Matriz(mat.length, m.mat[0].length);
        
        if (mat.length != m.mat[0].length){
            throw new ProdMatrizesIncompativeisException(this,m);
        }
        
        for (int lin = 0; lin < mat.length; lin++){
            for (int cols = 0; cols < m.mat[0].length; cols++){
                double sum = 0;
                for (int lins = 0; lins < m.mat.length; lins++){
                    sum += mat[lin][lins]*m.mat[lins][cols];
                }
                mult.mat[lin][cols] = sum;
            }
        }
        return mult;
    }

    /**
     * Retorna uma representação textual da matriz.
     * Este método não deve ser usado com matrizes muito grandes
     * pois não gerencia adequadamente o tamanho do string e
     * poderia provocar um uso excessivo de recursos.
     * @return Uma representação textual da matriz.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (double[] linha: mat) {
            s.append("[ ");
            for (double x: linha) {
                s.append(x).append(" ");
            }
            s.append("]");
        }
        return s.toString();
    }
    
}
