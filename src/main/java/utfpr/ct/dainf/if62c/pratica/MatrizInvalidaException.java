/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author JoaoVitorBF
 */
public class MatrizInvalidaException extends Exception {
    private final Matriz m1;

    public MatrizInvalidaException(Matriz m1) {
        super("Matriz de "+m1.getMatriz().length+"x"+m1.getMatriz()[0].length+" não pode ser criada");
        this.m1 = m1;
    }
    
    public int getNumLinhas() {
        return m1.getMatriz().length;
    }
    
    public int getNumColunas() {
        return m1.getMatriz()[0].length;
    }
}
